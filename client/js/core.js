var app = angular.module('App', ['ngRoute', 'LocalStorageModule', 'ngAnimate', 'ngclipboard']);

app.config(['$routeProvider', function($routeProvider) {
	$routeProvider
	.when('/home', {
		controller   : 'HomeCtrl', 
		controllerAs : 'ch', 
		templateUrl  : 'client/js/home/home.tmpl.html'
	})
	.when('/contacts', {
		controller   : 'ContactsCtrl', 
		controllerAs : 'cc', 
		templateUrl  : 'client/js/contacts/contacts.tmpl.html'
	})
	.otherwise({
		redirectTo : '/home'
	})
}])
.directive('focusMe', FocusDirective)
.service('AuthService', AuthService)
.service('ContactsService', 		ContactsService)
.controller('HomeCtrl', 		HomeController)
.controller('ContactsCtrl', 	ContactsController)
.run(['$location', function ($location) {
	if (localStorage['oauth2_sfdc-sb'] || localStorage['oauth2_sfdc']) {
		$location.path('/contacts');
		//$location.path('/home');
	} else {
		$location.path('/home');
	}
	console.log(localStorage);
}]);


