ContactsController.$inject = ['$scope', '$window', 'ContactsService', 'AuthService'];


function ContactsController ($scope, $window, ContactsService, AuthService) {
	var ctrl = this;
	ctrl.model = {
		contacts : [],
		filter 			: '',
		descr 			: ''
	}

	ctrl.util = {
		loading   			: false,
		clicked	  			: false,
		ctrlDown  			: false,
		shiftDown				: false,
		altDown					: false,
		descrModal			: false,
		option					: 'tab',  // { 'tab', 'window', 'incognito' }
		credentialsId 	: '',	
		showDetails			: false,
		showDetailsFor  : '',
		showInfo				: false
	}

	setTimeout(function() {ctrl.util.descrModal	= false;}, 1000);

	ctrl.goHome = function () {
		ContactsService.goHome();
	}

	ctrl.showDetails = function (index) {
		ctrl.util.showDetailsFor = index;
		ctrl.util.showDetails = true;
	}	

	ctrl.logOut = function () {
    if (localStorage['oauth2_sfdc-sb']) {
			delete localStorage["oauth2_sfdc-sb"];
		} else {
			delete localStorage["oauth2_sfdc"];
		}
		delete localStorage["oauth2_adapterReverse"];
    location.reload();
	}


	ctrl.init = function() {
		ctrl.util.loading = true;
		var domain = (localStorage['oauth2_sfdc-sb']) ? 'sfdc-sb' : 'sfdc';

		ContactsService.refreshToken(domain)
		.then(
			function (data) {
				return ContactsService.getContacts();
			}
		)
		.then (
			function (data) {
				console.log(data);
				ctrl.model.contacts = data.data;
				ctrl.util.loading = false;
			},
			function (err) {
				console.log(err);
				ctrl.util.loading = false;
			}
		);								
	}
	
	ctrl.init();
}