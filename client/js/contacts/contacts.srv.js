
ContactsService.$inject = ['$q', '$http', 'AuthService'];

function ContactsService ($q, $http, AuthService) {
	var deferred = $q.defer();

	this.getContacts = function() {
		$http({
		  method 	: 'GET',
		  url 		: AuthService.getUrl() + '/services/apexrest/ndrl/contacts',
		  headers : {'Authorization': 'Bearer ' + AuthService.getAccessToken()}
		}).then(
			function(data) {
				console.log(data);
				deferred.resolve(data);
			},
			function(err) {
				console.log(err);
				deferred.reject(err);
			}
		);
		return deferred.promise;
	};

	this.goHome = function() {
		var lsConfig = localStorage['oauth2_sfdc'];
		var objConfig = JSON.parse(lsConfig);
		var link = objConfig['instance_url'] + '/secur/frontdoor.jsp?sid=' + objConfig['access_token'];
		chrome.tabs.create({url:link});
	}

	this.refreshToken = function(domain) {
		var lsConfig = localStorage['oauth2_' + domain];
		var objConfig = JSON.parse(lsConfig);
		console.log(objConfig);
		var params = {
			'grant_type' 		: 'refresh_token',
			'refresh_token' : encodeURI(objConfig['refresh_token']),
			'client_id' 		: encodeURI(objConfig['clientId']),
			'client_secret' : encodeURI(objConfig['clientSecret'])
		}
		var refreshUrl = (domain == 'sfdc') 
			? 'https://login.salesforce.com' 
			: 'https://test.salesforce.com';
		refreshUrl += '/services/oauth2/token';
		var deferred = $q.defer();
		$http({
		  method 	: 'POST',
      	  data 		: params,
		  url 		: refreshUrl,
		  headers : {'Content-Type': 'application/x-www-form-urlencoded'},
		  transformRequest: function(obj) {
        var str = [];
        for(var p in obj)
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
    	},
		}).then(
			function(data) {
				console.log(data);
				objConfig['access_token'] = data.data.access_token;
				objConfig['accessToken'] = data.data.access_token;
				objConfig['issued_at'] = data.data.issued_at;
				localStorage['oauth2_' + domain] = JSON.stringify(objConfig);
				deferred.resolve(data);
			},
			function(err) {
				console.log(err);
				deferred.reject(err);
			}
		);
		return deferred.promise;
	}

}
