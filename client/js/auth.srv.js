AuthService.$inject = [];
function AuthService() {
	this.getAccessToken = function() {
		var domain = (localStorage['oauth2_sfdc-sb']) ? 'sfdc-sb' : 'sfdc';
		var config = JSON.parse(localStorage['oauth2_' + domain]);
		return config['access_token'];
	}

	this.getUrl = function () {
		var domain = (localStorage['oauth2_sfdc-sb']) ? 'sfdc-sb' : 'sfdc';
		var config = JSON.parse(localStorage['oauth2_' + domain]);
		return config['instance_url'];
	}

}